const amqp = require('amqplib');
const config = require('./config');
const decrementInventory = require('./middleware/decrementInventory');

//1. connect to rmq server
//2. create new channel
//3. create exchange
//4. create queue
//5. bind queue to exchange
//6. consume messages from the queue

const Consumer = {
  async consumeMessages() {
    const connection = await amqp.connect(config.rabbitMq.url);

    const channel = await connection.createChannel();

    await channel.assertExchange(config.rabbitMq.exchanges.orders, 'fanout');

    const q = await channel.assertQueue('InventoryQueue');

    await channel.bindQueue(
      q.queue,
      config.rabbitMq.exchanges.orders,
      config.rabbitMq.events.placeOrder
    );

    channel.consume(q.queue, (msg) => {
      const data = JSON.parse(msg.content); // OK
      // console.log(data); // OK
      channel.ack(msg);
      decrementInventory(data);
    });
  },
};

module.exports = Consumer;
