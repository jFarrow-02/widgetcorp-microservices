module.exports = {
  rabbitMq: {
    url: 'amqp://localhost',
    exchanges: {
      orders: 'orderExchange',
    },
    events: {
      placeOrder: 'PlaceOrderEvent',
    },
  },
};
