const Order = require('./Order');

class QueueMsg {
  order;
  eventType;

  constructor(msgObj) {
    const {
      order: { customerEmail, placedOnDate, widgets },
      eventType,
    } = msgObj;
    this.order = new Order(customerEmail, placedOnDate, widgets);
    this.eventType = eventType;
  }

  get order() {
    return this.order;
  }

  get eventType() {
    return this.eventType;
  }

  set order(o) {
    this.order = o;
  }

  set eventType(e) {
    this.eventType = e;
  }
}

module.exports = QueueMsg;
