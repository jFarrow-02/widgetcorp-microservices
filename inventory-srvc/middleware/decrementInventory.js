const { MongoClient } = require('mongodb');
const dbConfig = require('../db/dbConfig.json');

async function decrementInventory(msg) {
  const {
    order: { widgets },
    eventType,
  } = msg;

  try {
    if (eventType === 'PlaceOrderEvent') {
      const client = new MongoClient(
        `mongodb://${process.env.WIDGETCORP_MONGO_USER}:${process.env.WIDGETCORP_MONGO_PASSWORD}@${dbConfig.dbHost}:${dbConfig.dbPort}`
      );

      const connection = await client.connect();

      const db = client.db(dbConfig.dbName);

      console.log(msg);

      for (let size in widgets) {
        console.log(widgets[size]);
        const response = await db.collection(dbConfig.collection).updateOne(
          {
            name: dbConfig.orderToSchemaMap[size],
          },
          { $inc: { availableQty: -`${widgets[size]}` } }
        );
        console.log(response);
      }

      connection.close();
      return true;
    }
  } catch (err) {
    console.log(err.message);
  }

  // throw new Error('unknown event type');
}

module.exports = decrementInventory;
