const MongoClient = require('mongodb').MongoClient;
const { mongoHost } = require('../config');

async function saveCustomerOrder(order) {
  try {
    const client = new MongoClient(
      `mongodb://${process.env.WIDGETCORP_MONGO_USER}:${process.env.WIDGETCORP_MONGO_PASSWORD}@${mongoHost}:27017`
    );

    const collection = client
      .db('customer-orders')
      .collection('customer_orders');

    const orderObj = JSON.parse(order);
    const result = await collection.updateOne(
      { customerEmail: { $eq: orderObj.order.customerEmail } },
      {
        $push: {
          orders: {
            placedOnDate: orderObj.order.placedOnDate,
            widgets: orderObj.order.widgets,
          },
        },
      },
      { upsert: true }
    );
    console.log('result:', result); // OK
    client.close();
  } catch (err) {
    console.log(err.message);
  }
}

module.exports = saveCustomerOrder;
