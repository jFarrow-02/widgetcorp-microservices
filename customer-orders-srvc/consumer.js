const amqp = require('amqplib');
const config = require('./config');
const saveCustomerOrder = require('./middleware/saveCustomerOrder');

const Consumer = {
  async consumeMessages() {
    // 1. connect to rmq server
    const connection = await amqp.connect(config.rmqUrl);

    //2. create new channel
    const channel = await connection.createChannel();

    //3. create exchange
    await channel.assertExchange(config.exchanges.orders, 'fanout');

    //4. create queue
    const q = await channel.assertQueue('OrdersQueue');

    //5. bind queue to exchange
    await channel.bindQueue(
      q.queue,
      config.exchanges.orders,
      config.events.placeOrder
    );

    //6. consume messages from the queue
    channel.consume(q.queue, (order) => {
      const data = JSON.parse(order.content);
      console.log(data); // OK
      channel.ack(order); // Notify rmq that message has been received
      switch (data.eventType) {
        case config.events.placeOrder:
          saveCustomerOrder(JSON.stringify(data));
      }
    });
  },
};

module.exports = Consumer;
