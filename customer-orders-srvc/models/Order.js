class Order {
  customerEmail;
  placedOnDate;
  widgets;

  constructor(email, placedDate, widgets) {
    this.customerEmail = email;
    this.placedOnDate = placedDate;
    this.widgets = widgets;
  }

  get customerEmail() {
    return this.customerEmail;
  }

  get placedOnDate() {
    return this.placedOnDate;
  }

  get widgets() {
    return this.widgets;
  }

  set customerEmail(email) {
    this.customerEmail = email;
  }

  set placedOnDate(date) {
    this.placedOnDate = date;
  }

  set widgets(widgets) {
    this.widgets = widgets;
  }
}

module.exports = Order;
