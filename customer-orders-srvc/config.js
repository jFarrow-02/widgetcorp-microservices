module.exports = {
  rmqUrl: 'amqp://localhost',
  exchanges: {
    orders: 'orderExchange',
  },
  events: {
    placeOrder: 'PlaceOrderEvent',
  },
  mongoHost: '52.90.71.28',
};
