#!/bin/bash

sudo apt update -y

sudo apt install docker.io -y

sudo useradd mongo-admin

sudo usermod -aG docker mongo-admin

sudo su mongo-admin

docker run -d -p 27017:27017 --name mongo-server \
-e MONGO_INITDB_ROOT_USERNAME=mongoadmin \
-e MONGO_INITDB_ROOT_PASSWORD={INSERT_PASSWORD_HERE} \
mongo:latest

