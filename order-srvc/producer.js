const amqp = require('amqplib');
const config = require('./config');

const {
  rabbitMq: { url, exchange },
} = config;

const Producer = {
  channel: null,

  async createChannel() {
    // 1. Connect to rabbitmq
    const connection = await amqp.connect(url);
    //2. Create a channel
    this.channel = await connection.createChannel();
  },

  async publishMessage(routingKey, order) {
    if (!this.channel) {
      await this.createChannel();
    }
    //3. Create exchange
    await this.channel.assertExchange(exchange, 'fanout');

    // Put the message onto the exchange
    await this.channel.publish(
      exchange,
      routingKey,
      Buffer.from(JSON.stringify(order))
    );

    console.log(`The new ${routingKey} is sent to exchange ${exchange}`);
  },
};

module.exports = Producer;
