module.exports = {
  rabbitMq: {
    url: 'amqp://localhost',
    exchange: 'orderExchange',
    events: {
      placeOrder: 'PlaceOrderEvent',
    },
  },
};
