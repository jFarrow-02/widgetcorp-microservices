const MongoClient = require('mongodb').MongoClient;
const dbConfig = require('../db/dbConfig.json');

async function saveOrder(orderData) {
  try {
    const url = `mongodb://${process.env.WIDGETCORP_MONGO_USER}:${process.env.WIDGETCORP_MONGO_PASSWORD}@${dbConfig.dbHost}:${dbConfig.dbPort}`;

    const client = new MongoClient(url);

    const connection = await client.connect();

    const db = client.db(dbConfig.dbName);

    const response = await db
      .collection(dbConfig.collectionName)
      .insertOne(orderData);

    console.log('response:', response);

    connection.close();
  } catch (err) {
    console.log(err.message);
  }
}

module.exports = saveOrder;
