const express = require('express');
const server = express();
const bodyParser = require('body-parser');
const saveOrder = require('./middleware/saveOrder');
const Order = require('./models/Order');
const Producer = require('./producer');
const config = require('./config');
const PORT = 8000;

const {
  rabbitMq: {
    events: { placeOrder },
  },
} = config;

server.use(bodyParser.json('application/json'));

server.post('/place-order', async (req, res) => {
  if (!req.body.customerEmail || !req.body.widgets) {
    res.status(400).json({ status: 400, msg: 'bad request' });
  }
  const {
    body: { customerEmail, widgets },
  } = req;

  const order = new Order(customerEmail, new Date(Date.now()), widgets);
  await saveOrder(order);
  await Producer.publishMessage(config.rabbitMq.events.placeOrder, {
    order,
    eventType: placeOrder,
  });
  res.status(201).json({ status: 201, eventType: placeOrder, order });
});

server.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
