class Widget {
  size;
  listPrice;

  constructor(size, price) {
    this.size = size;
    this.price = price;
  }

  get size() {
    return this.size;
  }

  get price() {
    return this.price;
  }

  set size(size) {
    this.size = size;
  }

  set price(price) {
    this.price = price;
  }
}

module.exports = Widget;
